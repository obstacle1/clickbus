<?php

namespace App\Http\Controllers\clickbus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use App\Http\Controllers\Services\CreateIDManager AS IDManager;

use App\Models\clickbus\User AS User;

class UserController extends Controller
{
    // Get users info
    public function getUsers() {
      $users = User::where( 'status' , 1 )->get();
      return response()->json( $users );
    }

    // Get user info
    public function getUser( $userID ) {
      $user = User::find( $userID );
      return response()->json( $user );
    }

    // Update user
    public function updateUser( Request $request ) {
      try{
        $user = User::find( $request->id );
        $user->first_name   = $request->first_name;
        $user->last_name    = $request->last_name;
        $user->age          = $request->age;
        $user->birthday     = $request->birthday;
        $user->phone_number = $request->phone_number;
        $us        = $request->email;
        $user->save();

        return response()->json( [ "code" => 201 , "message" => "User updated" ] );
      } catch( Exception $e ) {
        return response()->json( [ "code" => 400 , "message" => "Error " . $e->getMessage() ] );
      }
    }

    // Add user
    public function addUser( Request $request ) {
      try{
        $user = new User;
        $user->id           = IDManager::userID();
        $user->first_name   = $request->first_name;
        $user->last_name    = $request->last_name;
        $user->age          = $request->age;
        $user->birthday     = $request->birthday;
        $user->phone_number = $request->phone_number;
        $user->email        = $request->email;
        $user->status       = 1;
        $user->save();

        return response()->json( [ "code" => 201 , "message" => "User added" , "id" =>  $user->id ] );
      } catch( Exception $e ) {
        return response()->json( [ "code" => 400 , "message" => "Error " . $e->getMessage() ] );
      }
    }
}
