<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Users routes
Route::get( '/getUsers' , 'clickbus\UserController@getUsers' );
Route::get( '/getUser/{userID}' , 'clickbus\UserController@getUser' );
Route::post( '/addUser' , 'clickbus\UserController@addUser' );
Route::post( '/updateUser' , 'clickbus\UserController@updateUser' );

// Accounts routes
Route::get( '/getAccount/{accountID}' , 'clickbus\AccountController@getAccount' );
Route::post( '/updateAccount' , 'clickbus\AccountController@updateAccount' );
Route::post( '/addAccount' , 'clickbus\AccountController@addAccount' );

// Movements routes
Route::post( '/deposit' , 'clickbus\MovementController@deposit' );
Route::post( '/payment' , 'clickbus\MovementController@payment' );
Route::post( '/withdraw' , 'clickbus\MovementController@withdraw' );
