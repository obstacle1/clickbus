Test ClickBus

La prueba está desarrollada sobre Laravel 5.8, los controladores se ubican en:
App/Http/Controllers/clickbus

Además, hay una carpeta con servicios adicionales para las diferentes acciones:
App/Http/Controllers/Services

Los modelos se ubican en:
App/Models/clickbus

En ellos utilicé las convenciones de Eloquent por lo que verán poca configuración.

Comparto adicionalmente una url de una coleccion que generé a través de Postman para realizar las pruebas.
https://www.getpostman.com/collections/bba32e1972f7c8bcce08