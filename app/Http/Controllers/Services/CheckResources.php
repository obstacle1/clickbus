<?php

namespace App\Http\Controllers\Services;

use App\Models\clickbus\Account AS Account;

class CheckResources {

    public function checkResources( $account_id , $amount ) {
        $check = Account::find( $account_id );
        if( $check->type == 1 ) {
            return ( $check->amount_available >= $amount );
        } 
        
        if( $check->type == 2 ) {
            return ( $check->amount_available >= ( $amount + ( $amount * .10 ) ) );
        }
    }

}