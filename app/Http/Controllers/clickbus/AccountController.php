<?php

namespace App\Http\Controllers\clickbus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Services\CreateIDManager AS IDManager;
use Exception;

use App\Models\clickbus\Account AS Accounts;

class AccountController extends Controller
{
    // Get account info
    public function getAccount( $account_id ) {
        $account = Accounts::find( $account_id );
        return response()->json( $account );
    }

    // Update account info
    public function updateAccount( Request $request ) {
        try{
            $account = Accounts::find( $request->id );
            $account->name = $request->name;
            if( $account->type == 1 && $request->has( 'amount_available' ) ) {
                $account->amount_available = $request->amount_available;
            } elseif( $account->type == 2 && $request->has( 'credit_limit' ) ) {
                $account->credit_limit = $request->credit_limit;
            }
            $account->save();
            return response()->json( [ "code" => 201 , "message" => "Account updated" ] );
        } catch( Exception $e ) {
            return response()->json( [ "code" => 400 , "message" => "Error " . $e->getMessage() ] );
        }
    }

    // Add account
    public function addAccount( Request $request ) {
        try{
            $account = new Accounts();
            $account->id = IDManager::accountID();
            $account->userID = $request->user_id;
            $account->name = $request->name;
            $account->type = $request->type;
            if( $request->type == 1 ) {
                $account->amount_available = $request->amount_available;
            }
            if( $request->type == 2 ) {
                $account->credit_limit = $request->credit_limit;
                $account->amount_available = $request->amount_available;
            }            
            $account->status = 1;
            $account->save();
            return response()->json( [ "code" => 201 , "message" => "Account added" , "id" => $account->id ] );
        } catch( Exception $e ) {
            return response()->json( [ "code" => 400 , "message" => "Error " . $e->getMessage() ] );
        }
    }


}
