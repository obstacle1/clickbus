<?php

namespace App\Models\clickbus;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    // Timestamps
    public $timestamps = false;
}
