<?php

namespace App\Http\Controllers\clickbus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Services\ApplyMovement AS ApplyMovement;
use App\Http\Controllers\Services\CheckResources AS CheckResources;

use App\Models\clickbus\Account AS Account;

use Exception;

class MovementController extends Controller
{

    protected $movement;
    protected $resources;

    public function __construct( ApplyMovement $applyMovement , CheckResources $resources ) {
        $this->movement = $applyMovement;
        $this->resources = $resources;
    }

    // Action to execute a withdraw
    public function withdraw( Request $request ) {
        $account = Account::find( $request->account_id );
        if( !$account ) {
            return response()->json( [ "code" => 400 , "message" => "Non-existent account" ] );
        }


        if( ! $this->resources->checkResources( $request->account_id , $request->amount ) ){
            return response()->json( [ "code" => 400 , "message" => "Insufficient funds" ] );
        } else {
            return $this->movement->applyMovement( $request , 1 , $account->type );
        }
    }

    // Payment to credit account
    public function payment( Request $request ) {
        $account = Account::find( $request->account_id );
        if( !$account ) {
            return response()->json( [ "code" => 400 , "message" => "Non-existent account" ] );
        }

        /* Operation allowed only for credit accounts */
        if( $account->type != 2 ) {
            return response()->json( [ "code" => 400 , "message" => "Invalid operation" ] );
        } else if( $account->credit_limit == $account->amount_available ) {
            return response()->json( [ "code" => 200 , "message" => "No payment needed"  ] );
        } else {
            try{
                return $this->movement->applyMovement( $request , 3 , $account->type );
            } catch( Exception $e ) {
                return response()->json( [ "code" => 400 , "message" => "Error " . $e->getMessage() ] );
            }
        }
    }

    // Deposit to debit account
    public function deposit( Request $request ) {
        $account = Account::find( $request->account_id );
        if( !$account ) {
            return response()->json( [ "code" => 400 , "message" => "Non-existent account" ] );
        }
        /* Operation allowed only for debit accounts */
        if( $account->type != 1 ) {
            return response()->json( [ "code" => 400 , "message" => "Invalid operation" ] );
        } else {
            try{
                return $this->movement->applyMovement( $request , 2 , $account->type );
            } catch( Exception $e ) {
                return response()->json( [ "code" => 400 , "message" => "Error " . $e->getMessage() ] );
            }
        }        
    }
}
