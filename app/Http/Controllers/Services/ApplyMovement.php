<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Services\FundUpdate AS FundUpdate;
use App\Models\clickbus\Movement AS Movement;

class ApplyMovement {

    protected $fund;

    public function __construct( FundUpdate $fund ) {
        $this->fund = $fund;
    }

    /**
     *Apply movement to specific account
     * @var $type 1)Withdraw 2)Deposit 3)Payment
     */
    public function applyMovement( $request , $movement_type , $account_type ) { 
        try{
            $comision = 0;
            if( $movement_type == 1 && $account_type == 2 ) {
                $comision = $request->amount * .10;
            }
            $deposit = new Movement;
            $deposit->account_id     = $request->account_id;
            $deposit->amount         = $request->amount;
            $deposit->type           = $movement_type;
            $deposit->comision       = $comision;            
            $deposit->date_operation = date( 'Y-m-d H:i:s' );
            $deposit->status         = 1;
            if( $deposit->save() ) {
                $this->fund->updateAccount( $request->account_id , ($request->amount + $comision ), $movement_type );
            }
            return response()->json( [ "code" => 201 , "message" => "Successful operation" ] );
        } catch( Exception $e ) {
            return response()->json( [ "code" => 400 , "message" => "Error " . $e->getMessage() ] );
        }        
    }

}
