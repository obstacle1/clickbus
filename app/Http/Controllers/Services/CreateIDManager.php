<?php

namespace App\Http\Controllers\Services;

class CreateIDManager {

    // Create ID Account
    public static function accountID() {
        return rand( 1111111111 , 9999999999 );
    }

    // Create ID User
    public static function userID() {
        return rand( 1111111 , 9999999 );
    }

}
