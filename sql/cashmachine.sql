-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: cashmachine
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` varchar(18) NOT NULL,
  `userID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` int(11) NOT NULL,
  `credit_limit` double DEFAULT '0',
  `amount_available` double DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES ('2948717022',3,'Nomina',1,0,0,1,'2019-08-21 05:11:12','2019-08-21 05:11:12'),('3772289318',2,'Mis Ahorros',1,NULL,0,1,'2019-08-21 05:07:58','2019-08-21 05:07:58'),('9243191570',1,'Ahorro para vacaciones',1,NULL,2194.5599999999995,1,NULL,'2019-08-22 04:27:02'),('9248590362',1,'Tarjeta de credito',2,10000,6000,1,NULL,'2019-08-22 04:32:44'),('9911880161',4,'La poderosa',2,30000,30000,1,'2019-08-21 05:21:24','2019-08-21 05:21:24');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_type`
--

DROP TABLE IF EXISTS `accounts_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_type`
--

LOCK TABLES `accounts_type` WRITE;
/*!40000 ALTER TABLE `accounts_type` DISABLE KEYS */;
INSERT INTO `accounts_type` VALUES (1,'Credit',1),(2,'Debit',1);
/*!40000 ALTER TABLE `accounts_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movements`
--

DROP TABLE IF EXISTS `movements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(18) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `comision` double DEFAULT NULL,
  `date_operation` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movements`
--

LOCK TABLES `movements` WRITE;
/*!40000 ALTER TABLE `movements` DISABLE KEYS */;
INSERT INTO `movements` VALUES (7,'9243191570',2,2000,0,'2019-08-22 03:38:05',1),(8,'9243191570',2,1500,0,'2019-08-22 03:39:37',1),(9,'9243191570',2,794.56,0,'2019-08-22 03:40:39',1),(10,'9243191570',2,600,0,'2019-08-22 04:00:54',1),(11,'9243191570',2,800,0,'2019-08-22 04:12:15',1),(13,'9243191570',1,3500,0,'2019-08-22 04:27:02',1),(14,'9248590362',1,5000,500,'2019-08-22 04:29:53',1),(15,'9248590362',3,1500,0,'2019-08-22 04:32:44',1);
/*!40000 ALTER TABLE `movements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movements_type`
--

DROP TABLE IF EXISTS `movements_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movements_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movement` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movements_type`
--

LOCK TABLES `movements_type` WRITE;
/*!40000 ALTER TABLE `movements_type` DISABLE KEYS */;
INSERT INTO `movements_type` VALUES (1,'Withdraw',1),(2,'Deposit',1),(3,'Payment',1);
/*!40000 ALTER TABLE `movements_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` varchar(8) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `age` varchar(45) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`id`),
  UNIQUE KEY `uniqueD` (`first_name`,`last_name`,`birthday`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('4074277','Leticia','Salazar','57','1962-09-19','7223901782','letysalazar@gmail.com',1,NULL,NULL),('5257749','José','Gutiérrez','36','1983-07-25','2299887766','ositogg@gmail.como',1,NULL,NULL),('5711271','Claudia','Reyes','37','1982-10-15','7223901790','clau.reyes.salazar@gmail.com',1,'2019-08-21 04:52:05','2019-08-21 04:52:05'),('8809671','Lorenzo','Reyes','4','2014-12-29','5578100961','lorenzo.job.2912@gmail.com',1,NULL,NULL),('9748171','Elizabeth','Cuellar','37','1982-04-08','7223501748','eliza.cuellar@gmail.com',1,NULL,NULL),('9940560','Carlitos','ReyeSalazar','35','1984-03-17','7122032343','cvreyes.salazar@gmail.com',1,NULL,'2019-08-21 04:50:09');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-21 23:49:03
