<?php

namespace App\Http\Controllers\Services;

use App\Models\clickbus\Movement AS Movement;
use App\Models\clickbus\Account AS Account;

class FundUpdate {

    public function updateAccount( $acount_id , $amount , $type ) {
        $accountUpdate = Account::find( $acount_id );
        $accountUpdate->amount_available = ( ( $type == 1 ) ? $accountUpdate->amount_available - $amount : $accountUpdate->amount_available + $amount );
        $accountUpdate->save();
    }

}